package com.wemirr.framework.commons.i18n;

/**
 * @author Levin
 */
public interface Language {


    /**
     * 国际化关键字
     *
     * @return 国际化关键字
     */
    String getLanguage();

}
